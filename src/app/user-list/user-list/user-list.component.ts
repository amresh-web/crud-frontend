import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  public userLists: any = [];
  first = 0;
  rows = 10;
  display: boolean = false;
  toDOForm: FormGroup;
  editData:any = [];
  public isEditReady: boolean = false;

  constructor(private userservice: UserService, private fb: FormBuilder,private messageService: MessageService) {
    this.toDOForm = this.fb.group({
      cname: new FormControl('',[Validators.required]),
      desc: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.userList();
  }

  userList() {
    this.userservice.getUserList().subscribe((data) => {
      this.userLists = data;
      console.log(this.userLists);
    });
  }

  
  addUser(){
    this.display = true;
  }

  onSubmit(){
    let formValue = this.toDOForm.value;
    
    let formData = {};

    formData['name'] = formValue['cname'];
    formData['description'] = formValue['desc'];

    if(this.isEditReady){
      this.userservice.updateToDoById(this.toIds,formData).subscribe((res) => {
        this.messageService.add({severity:'success', summary:'Success Message', detail:'Updated Successfully'});
        this.userList();
        this.toDOForm.reset();
        setTimeout(() =>{
          this.display = false;
        },1000);
      });
    } else {
    this.userservice.addToDo(formData).subscribe((res) => {
      this.messageService.add({severity:'success', summary:'Success Message', detail:'Added Successfully'});
      console.log('Added');
      this.toDOForm.reset();
      this.userList();
      setTimeout(() =>{
        this.display = false;
      },1000);
    });
  }
    console.log(formData)
  }

  public toIds;
  editUser(id){
    let todoId = id;
    this.toIds = id;
    this.display = true;
    this.userservice.getToDoById(todoId).subscribe((data) => {
      this.editData = data;

      this.toDOForm.patchValue({
        cname : this.editData['name'],
        desc: this.editData['description']
      });
      
      this.isEditReady = true;
      console.log(this.editData);
    });
    
  }

  delete(id){
    let todoId = id;
    this.userservice.deleteToDo(todoId).subscribe((res) => {
      this.messageService.add({severity:'success', summary:'Success Message', detail:'Deleted Successfully'});
      this.userList();
    });
  }

  cancel(){
    this.display = false;
    this.isEditReady = false;
    this.toDOForm.reset();
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.userLists
      ? this.first === this.userLists.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.userLists ? this.first === 0 : true;
  }

  

}
