import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public baseUrl = environment.baseUrl;

  constructor(private httpclient: HttpClient) { }

  getUserList(){
    return this.httpclient.get(`${this.baseUrl}/todos`)
  }

  addToDo(data){
    return this.httpclient.post(`${this.baseUrl}/todos`,data)
  }

  getToDoById(toid){
    return this.httpclient.get(`${this.baseUrl}/todos/${toid}`)
  }

  updateToDoById(id,data){
    console.log(data)
    return this.httpclient.put(`${this.baseUrl}/todos/${id}`,data)
  }

  deleteToDo(id){
    return this.httpclient.delete(`${this.baseUrl}/todos/${id}`)
  }
}
