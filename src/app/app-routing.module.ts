import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {path: 'user-list', loadChildren: () => import('./user-list/user-list.module').then(m=>m.UserListModule)},
    {path: '', redirectTo:'user-list',pathMatch:'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
export class AppRoutingModule { } 